#Import Django
from django.utils import timezone

#Definimos constantes
BEGDA = timezone.datetime(2019, 1, 1)
ENDDA = timezone.datetime(9999, 12, 31)
#Faltantes
NOMAIL = 'sinemail@nomail.com'
NODOM = 'SINDOMICILIO'
NOTEL = 'SINTELEFONO'