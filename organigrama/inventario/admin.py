#Imports Django
from django.contrib import admin
#Import extras
#Import Personales
from .models import Rubro, Item
from .forms import ItemForm

#Particularidades
class RubroAdmin(admin.ModelAdmin):
    model = Rubro
    search_fields = ['nombre']

class ItemAdmin(admin.ModelAdmin):
    model = Item
    search_fields = ['nombre', 'organismo__nombre']
    list_filter = ['rubro', 'activo']
    form = ItemForm

# Register your models here.
admin.site.register(Rubro, RubroAdmin)
admin.site.register(Item, ItemAdmin)