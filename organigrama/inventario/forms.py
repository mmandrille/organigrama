#Imports Django
from django.forms import ModelForm
#Imports Extras
from dal import autocomplete
#Imports del Proyecto
from core.widgets import XDSoftDatePickerInput, XDSoftDateTimePickerInput
#Imports de la app
from .models import Item

class ItemForm(ModelForm):
    class Meta:
        model = Item
        fields = '__all__'
        widgets = {
            'organismo': autocomplete.ModelSelect2(url='organismos:organismos-autocomplete'),
            'begda': XDSoftDatePickerInput(attrs={'autocomplete':'off'}),
            'endda': XDSoftDatePickerInput(attrs={'autocomplete':'off'}),
        }