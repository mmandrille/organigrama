#Imports django
from django.db import models
from django.utils import timezone
#Imports Extras
from tinymce.models import HTMLField
from auditlog.registry import auditlog
#Imports del proyecto
from organigrama.settings import LOADDATA
from organigrama.constantes import ENDDA
from organismos.models import Organismo

# Create your models here.
class Rubro(models.Model):
    padre = models.ForeignKey('Rubro', on_delete=models.SET_NULL, related_name='subrubros', blank=True, null=True)
    nombre = models.CharField('Nombre', max_length=100)
    descripcion = HTMLField(verbose_name='Descripcion', null=True, blank=True)
    def __str__(self):
        if self.padre:
            return self.nombre + ' (' + str(self.padre.nombre) + ')'
        return self.nombre

class Item(models.Model):
    organismo = models.ForeignKey(Organismo, on_delete=models.SET_NULL, related_name='items', blank=True, null=True)
    rubro = models.ForeignKey(Rubro, on_delete=models.SET_NULL, related_name='items', blank=True, null=True)
    nombre = models.CharField('Nombre', max_length=100)
    descripcion = HTMLField(verbose_name='Descripcion', null=True, blank=True)
    begda = models.DateField('Alta',  default=timezone.now)
    endda = models.DateField('Baja', default=ENDDA)
    activo = models.BooleanField(default=True)
    def __str__(self):
        return self.nombre + ' (' + str(self.rubro) + ')'

if not LOADDATA:
    #Auditoria
    auditlog.register(Rubro)
    auditlog.register(Item)