CARGOS =    (
    (0, 'Sin Cargo'),
    (1, 'Gobernador'), (2, 'Vice Gobernador'),
    (10, 'Ministro'),
    (20, 'Secretario'), (21, 'Secretaria'), (22, 'Subsecretario'), (23, 'SubScretaria'),
    (31, 'Director'), (32, 'Directora'), (33, 'SubDirector'), (34, 'SubDirectora'), 
    (41, 'Coordinador'), (42, 'Coordinadora'),
    (45, 'Administrativo'), (46, 'Contratado'),
    (51, 'Consejal'), (52, 'Diputado'), (53, 'Senador'), (54, 'Diputada'), (55, 'Senadora'),
    (61, 'Juez'), (62, 'Fiscal'), (63, 'Vocal'), (64, 'Procurador'), (65, 'Procuradora'), (66, 'Defensor'),
    (71, 'Intendente'), (72, 'Comisionado'),
    (81, 'Escribano'), (82, 'Presidente'), (83, 'Vice Presidente'),
    (91, 'Administrativo'), (92, 'Jefe'), (93, 'Obispo'), (94, 'Sacerdote'), (95, 'Parroco'), (97, 'Consul'), (99, 'Asesor')
)

TIPO_RECIBO = (
    ('REG', 'Recibo Basico'),
    ('SAC', 'Sueldo Anual complementario'),
)

NIVEL_EDUCATIVO = (
    (1, 'Educación Inicial'),
    (2, 'Educación Primaria'),
    (3, 'Educación Secundaria'),
    (4, 'Educación Superior'),
)

NIVEL_HABILIDAD = (
    (1, 'Inicial'),
    (2, 'Media'),
    (3, 'Avanzado'),
    (4, 'Experto'),
)