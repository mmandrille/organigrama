#Imports del proyecto
from .models import Individuo

#Definimos nuestras funciones
def get_individuo(request):
    try:
        return Individuo.objects.get(usuario=request.user)
    except:
        return None