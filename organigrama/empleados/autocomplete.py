#Imports django
from django.db.models import Q
#Imports Extras
from dal import autocomplete
#Imports de la app
from .models import Individuo, TipoHabilidad

class IndividuoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Individuo.objects.all()
        if self.q:
            qs = qs.filter(
                Q(apellidos__icontains=self.q)
                |
                Q(num_doc__startswith=self.q)
            )
        return qs


class TipoHabilidadAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = TipoHabilidad.objects.all()
        if self.q:
            qs = qs.filter(
                Q(nombre__icontains=self.q)
                |
                Q(area__nombre__icontains=self.q)
            )
        return qs