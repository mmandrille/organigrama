#Imports Django
from django.contrib import admin
from django.shortcuts import redirect
from django.utils.safestring import mark_safe
#Import extras
from nested_inline.admin import NestedStackedInline, NestedModelAdmin
#Import Personales
from .models import AreaConocimiento, TipoHabilidad
from .models import Individuo, Empleo, ReciboSueldo, Escolaridad, HabilidadAdquirida
from .forms import EmpleoForm, EmpleoOrgForm
#Particularidades
class ReciboSueldoInline(NestedStackedInline):
    model = ReciboSueldo
    fk_name = 'empleo'
    extra = 0

class EmpleoInline(NestedStackedInline):
    model = Empleo
    fk_name = 'individuo'
    inlines = [ReciboSueldoInline]
    form = EmpleoForm
    extra = 0

class EscolaridadInline(NestedStackedInline):
    model = Escolaridad
    fk_name = 'individuo'
    extra = 0

class HabilidadAdquiridaInline(NestedStackedInline):
    model = HabilidadAdquirida
    fk_name = 'individuo'
    extra = 0

class EmpleoOrgInline(admin.TabularInline):#Para Organismos//Otra app
    model = Empleo
    fk_name = 'organismo'
    form = EmpleoOrgForm
    extra = 0

class TipoHabilidadInline(NestedStackedInline):
    model = TipoHabilidad
    fk_name = 'area'
    extra = 0

#Admin registrados
class IndividuoAdmin(NestedModelAdmin):
    model = Individuo
    inlines = [EscolaridadInline, HabilidadAdquiridaInline, EmpleoInline]
    search_fields = ['num_doc', 'nombres', 'apellidos']
    list_filter = ['empleos__cargo', ]
    readonly_fields = ['panel', ]
    def panel(self, obj):
        url = redirect("empleados:ver_panel", individuo_id=obj.id).url
        return mark_safe("<a href='" + url + "' target='_blank'>Ver Panel</a>")
    panel.allow_tags = True

class AreaConocimientoAdmin(admin.ModelAdmin):
    model = AreaConocimiento
    search_fields = ['nombre']
    inlines = [TipoHabilidadInline]

# Register your models here.
admin.site.register(Individuo, IndividuoAdmin)
admin.site.register(AreaConocimiento, AreaConocimientoAdmin)