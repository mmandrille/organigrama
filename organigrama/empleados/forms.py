#Imports Django
from django import forms
from django.forms import ModelForm
from django.forms.widgets import TextInput
#Imports Extras
from dal import autocomplete
#Imports del Proyecto
from core.widgets import XDSoftDatePickerInput, XDSoftDateTimePickerInput
#Imports de la app
from .models import Individuo, Empleo
from .models import Escolaridad, HabilidadAdquirida

#Definimos Forms
class Pinform(forms.Form):
    pin = forms.CharField(label='Pin')

class DatosBasicosIndividuoForm(ModelForm):
    class Meta:
        model = Individuo
        fields = (
            'tipo_doc', 'num_doc',
            'apellidos', 'nombres',
            'fecha_nacimiento',
            'sexo',
            'telefono',
            'email',
            'cuil',
            'titulo',
        )
        widgets = {
            'fecha_nacimiento': XDSoftDatePickerInput(attrs={'autocomplete':'off'}),
        }

class FotoIndividuoForm(ModelForm):
    class Meta:
        model = Individuo
        fields = ('fotografia',)

class EmpleoForm(ModelForm):
    class Meta:
        model = Empleo
        fields = '__all__'
        widgets = {
            'organismo': autocomplete.ModelSelect2(url='organismos:organismos-autocomplete'),
        }

class EmpleoOrgForm(ModelForm):
    class Meta:
        model = Empleo
        fields = '__all__'
        widgets = {
            'individuo': autocomplete.ModelSelect2(url='empleados:individuos-autocomplete'),
        }

class EscolaridadForm(ModelForm):
    class Meta:
        model = Escolaridad
        fields = '__all__'
        exclude = ('individuo', )
        widgets = {
            'begda': XDSoftDatePickerInput(attrs={'autocomplete':'off'}),
            'endda': XDSoftDatePickerInput(attrs={'autocomplete':'off'}),
        }

class HabilidadAdquiridaForm(ModelForm):
    class Meta:
        model = HabilidadAdquirida
        fields = '__all__'
        exclude = ('individuo', )
        widgets = {
            'habilidad': autocomplete.ModelSelect2(url='empleados:habilidades-autocomplete'),
        }