#Imports de python
import json
import datetime
import logging
from base64 import b64decode
#Imports de Django
from django.utils import timezone
from django.http import JsonResponse
from django.contrib.auth import authenticate
from django.core.files.base import ContentFile
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
#Imports Extras
from fcm_django.models import FCMDevice
#Imports del proyecto
from core.functions import json_error
#Imports de la app
from .models import Individuo, ReciboSueldo

#Logger
logger = logging.getLogger('signals')

#Definimos nuestras APIS
@csrf_exempt
@require_http_methods(["POST"])
def recibir_recibo(request):
    try:
        data = request.POST
        #Optimizamos busqueda
        individuo = Individuo.objects.prefetch_related('empleos')
        #Buscamos el empleado:
        num_doc = data['dni'].upper()
        individuo = individuo.get(num_doc=num_doc)
        #Iniciamos la creacion del Recibo
        recibo = ReciboSueldo()
        #Tipo de recibo:
        recibo.tipo = int(data["tipo"])
        #Obtenemos fecha del recibo:
        recibo.fecha = datetime.date(#Formato: AAAAMMDD
            int(data["fecha"][0:4]),
            int(data["fecha"][4:6]),
            int(data["fecha"][6:8]),
        )
        #Asumimos empleo activo: (?)
        empleos = [e for e in individuo.empleos.all() if e.begda < recibo.fecha < e.endda]
        if empleos:
            recibo.empleo = empleos[0]#Le asignamos uno que este activo entre fechas
        #Obtenemos el archivo
        recibo.archivo = request.FILES['recibo']
        #Aclaracion:
        recibo.aclaracion = data["aclaracion"]
        #Lo guardamos
        recibo.save()
        return JsonResponse(
            {
                "accion":"recibir_recibo",
                "realizado": True,
            },
            safe=False
        )
    except Exception as e:
        return json_error(e, "foto_perfil", logger, data)