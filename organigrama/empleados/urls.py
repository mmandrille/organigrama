#Imports Django
from django.conf.urls import url
from django.urls import path
#imports del proyecto
from . import views
from . import autocomplete

app_name = 'empleados'
urlpatterns = [
    #Public:
    path('recibo/<int:recibo_id>', views.ver_recibo, name='ver_recibo'),
    #Personales
    path('', views.inicio, name='inicio'),
    
    path('panel/<int:individuo_id>', views.ver_panel, name='ver_panel'),

    path('add/escolaridad/<int:individuo_id>', views.agregar_escolaridad, name='agregar_escolaridad'),
    path('add/habilidad/<int:individuo_id>', views.agregar_habilidad, name='agregar_habilidad'),

    path('mod_datos/<int:individuo_id>', views.DatosBasicosIndividuo, name='mod_datos'),
    path('mod_foto/<int:individuo_id>', views.CambiarFotoIndividuo, name='mod_foto'),

    #Utilidad
    path('buscar/habilidad', views.buscar_por_habilidad, name='buscar_por_habilidad'),

    #Autocomplete
    url(r'^individuos-autocomplete/$', autocomplete.IndividuoAutocomplete.as_view(), name='individuos-autocomplete',),
    url(r'^habilidades-autocomplete/$', autocomplete.TipoHabilidadAutocomplete.as_view(), name='habilidades-autocomplete',),
]