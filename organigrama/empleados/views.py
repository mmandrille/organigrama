
#Imports Django
from django.shortcuts import render, redirect
from django.forms.forms import NON_FIELD_ERRORS
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
#Imports Proyecto
from .functions import get_individuo
from .models import AreaConocimiento, Individuo, ReciboSueldo
from .models import TipoHabilidad, HabilidadAdquirida
from .forms import DatosBasicosIndividuoForm, FotoIndividuoForm
from .forms import EscolaridadForm, HabilidadAdquiridaForm
from .forms import Pinform

# Create your views here.
def inicio(request):
    if request.user.is_staff:
        individuo = get_individuo(request)
        return render(request, 'menu_empleados.html', {
            'individuo': individuo,
        })
    else:
        return redirect('empleados:ver_panel')

def ver_panel(request, individuo_id=None):
    #Buscamos el individuo
    if request.user.is_staff and individuo_id:
        individuo = Individuo.objects.get(pk=individuo_id)
    else:
        individuo = get_individuo(request)
    #Luego de la busqueda
    if individuo:#Si obtuvimos individuo
        #Lanzamos Panel
        return render(request, 'panel_individuo.html', {
            'individuo': individuo,
            }
        )
    else:
        return render(request, 'sin_individuo.html', {})

def ver_recibo(request, recibo_id):
    recibo = ReciboSueldo.objects.get(id=recibo_id)
    form = Pinform()
    if request.method == 'POST':
        form = Pinform(request.POST)
        if form.is_valid():
            pin = form.cleaned_data['pin']
            if pin == recibo.get_pin():
                return redirect(recibo.archivo.url)
            else:
                form.add_error('pin', "El Pin no es Correcto, revise bajo el codigo QR.")#Si esta mal el pin
    #Lanzamos Form
    return render(request, 'extras/generic_form.html', {
        'form': form,
        'titulo': 'Inserte Pin de Seguridad',
        'boton': 'Ingresar'
    })

#Manejo de datos basicos
@login_required
def DatosBasicosIndividuo(request, individuo_id=None):
    individuo = Individuo.objects.get(pk=individuo_id)
    form = DatosBasicosIndividuoForm(instance=individuo)
    if request.method == 'POST':
        form = DatosBasicosIndividuoForm(request.POST, instance=individuo)
        if form.is_valid():
            form.save()#Guardamos los cambios
            return redirect('empleados:ver_panel', individuo_id=individuo.pk)
    return render(request, 'extras/generic_form.html', {'form': form, 'titulo': 'Datos Basicos', 'boton': 'Modificar'})

@login_required
def CambiarFotoIndividuo(request, individuo_id=None):
    individuo = Individuo.objects.get(pk=individuo_id)
    form = FotoIndividuoForm(instance=individuo)
    if request.method == 'POST':
        form = FotoIndividuoForm(request.POST, request.FILES, instance=individuo)
        if form.is_valid():
            form.save()#Guardamos los cambios
            return redirect('empleados:ver_panel', individuo_id=individuo.pk)
    return render(request, 'extras/generic_form.html', {'form': form, 'titulo': 'Fotografia', 'boton': 'Modificar'})

@login_required
def agregar_habilidad(request, individuo_id=None):
    form = HabilidadAdquiridaForm()
    if request.method == 'POST':
        form = HabilidadAdquiridaForm(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.individuo = Individuo.objects.get(pk=individuo_id)
            form.save()
            return redirect('empleados:ver_panel', individuo_id=form.individuo.pk)
    return render(request, 'extras/generic_form.html', {'form': form, 'titulo': 'Habilidad y Aptitudes', 'boton': 'Agregar'})

@login_required
def agregar_escolaridad(request, individuo_id=None):
    form = EscolaridadForm()
    if request.method == 'POST':
        form = EscolaridadForm(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.individuo = Individuo.objects.get(pk=individuo_id)
            form.save()
            return redirect('empleados:ver_panel', individuo_id=form.individuo.pk)
    return render(request, 'extras/generic_form.html', {'form': form, 'titulo': 'Historial Academico', 'boton': 'Agregar'})

#Utilidades
@staff_member_required
def buscar_por_habilidad(request):
    if request.method == "GET":
        areas = AreaConocimiento.objects.all()
        return render(request, 'buscar_por_habilidad.html', {
            'areas': areas,
            'has_table': True,
        })
    else:
        #Obtenemos las habilidades seleccionadas
        habilidades_buscadas = [int(id) for id in request.POST.getlist('habilidad')]
        individuos_preseleccionados = Individuo.objects.filter(habilidades__habilidad__in=habilidades_buscadas)#Solo los que tienen esas habilidades
        individuos_preseleccionados = individuos_preseleccionados.distinct()
        #Optimizamos
        individuos_preseleccionados = individuos_preseleccionados.prefetch_related('habilidades')
        #Preparamos la lista a entregar
        individuos = []
        for individuo in individuos_preseleccionados:
            individuo.puntaje = sum([h.nivel for h in individuo.habilidades.all() if h.habilidad.pk in habilidades_buscadas])
            individuos.append(individuo)
        #Ordenamos por fecha
        individuos.sort(key=lambda x: x.puntaje, reverse=True)
        #Lanzamos reporte
        return render(request, 'empleados_por_habilidad.html', {
            'habilidades_buscadas': habilidades_buscadas,
            'individuos': individuos,
        })
