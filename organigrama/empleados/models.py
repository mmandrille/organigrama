#Imports de python
#Imports django
from django.db import models
from django.utils import timezone
from django.core.validators import RegexValidator
from django.contrib.auth.models import User
#Imports Extra
from auditlog.registry import auditlog
#Imports del proyecto
from organigrama.settings import LOADDATA
from organigrama.constantes import ENDDA
from core.choices import TIPO_DOCUMENTOS, TIPO_SEXO
from organismos.models import Organismo
#Imports de la app
from .choices import CARGOS, TIPO_RECIBO, NIVEL_EDUCATIVO, NIVEL_HABILIDAD

# Create your models here.
class Individuo(models.Model):
    tipo_doc = models.IntegerField(choices=TIPO_DOCUMENTOS, default=2)
    num_doc = models.CharField('Numero de Documento/Pasaporte', 
        max_length=50,
        validators=[RegexValidator('^[A-Z_\d]*$', 'Solo Mayusculas.')],
        unique=True,
        db_index=True,
    )
    apellidos = models.CharField('Apellidos', max_length=100)
    nombres = models.CharField('Nombres', max_length=100)
    fecha_nacimiento = models.DateField(verbose_name="Fecha de Nacimiento", null=True, blank=True)
    sexo = models.CharField('Sexo', max_length=1, choices=TIPO_SEXO, default='M')
    telefono = models.CharField('Telefono', max_length=50, default='+549388', null=True, blank=True)
    email = models.EmailField('Correo Electronico Personal', null=True, blank=True)
    cuil = models.CharField('CUIL', max_length=13, null=True, blank=True)
    fotografia = models.FileField('Fotografia', upload_to='empleados/individuos/', null=True, blank=True)
    titulo = models.CharField('Titulo Profesional', max_length=50, null=True, blank=True)
    usuario = models.OneToOneField(User, on_delete=models.SET_NULL, null=True, blank=True, related_name="empleado")
    def __str__(self):
        return self.num_doc + ': ' + self.apellidos + ', ' + self.nombres

#Laboral
class Empleo(models.Model):
    organismo = models.ForeignKey(Organismo, on_delete=models.SET_NULL, related_name='empleados', blank=True, null=True)
    individuo = models.ForeignKey(Individuo, on_delete=models.CASCADE, related_name="empleos")
    cargo = models.IntegerField(choices=CARGOS, default=45)
    subcargo = models.CharField('SubCargo', max_length=20, blank=True, null=True)
    decreto = models.IntegerField(blank=True, null=True)
    funcion_unica = models.BooleanField(default=False)
    begda = models.DateField('Designacion',  default=timezone.now)
    endda = models.DateField('Cese de Funciones', default=ENDDA)
    activo = models.BooleanField(default=True)
    #categoria =
    def __str__(self):
        return self.get_cargo_display() + ' en ' + str(self.organismo.nombre)
    def save(self, *args, **kwargs):
        if self.activo == True and self.funcion_unica:#tener en cuenta otros funcionarios
            self.organismo.empleados.exclude(pk=self.pk).filter(cargo=self.cargo,subcargo=self.subcargo).update(endda=self.begda, activo=False)
        super(Empleo, self).save(*args, **kwargs)

class ReciboSueldo(models.Model):
    empleo = models.ForeignKey(Empleo, on_delete=models.CASCADE, related_name="recibos")
    tipo = models.CharField('Tipo Recibo', max_length=3, choices=TIPO_RECIBO, default='REG')
    fecha = models.DateField('Fecha Subido', default=timezone.now)
    archivo = models.FileField('Archivo', upload_to='recibos/')
    aclaracion = models.CharField('Aclaraciones', max_length=1000, blank=True)
    def __str__(self):
        return str(self.empleo) + ' (' + str(self.fecha) + ')'
    def get_pin(self):
        return self.empleo.individuo.num_doc#Se puede determinar otro pin mas eficiente.

#Escolaridad
class Escolaridad(models.Model):
    individuo = models.ForeignKey(Individuo, on_delete=models.CASCADE, related_name='escolaridad')
    nivel = models.IntegerField(choices=NIVEL_EDUCATIVO, default=3)
    titulo = models.CharField('Titulo Profesional', max_length=100)
    establecimiento = models.CharField('Establecimiento Educativo', max_length=100)
    archivo = models.FileField(verbose_name='Archivo', upload_to='personal/escolaridad/', blank=True, null=True)
    begda = models.DateField('Fecha Inicio',  default=timezone.now, blank=True, null=True)
    endda = models.DateField('Fecha Fin', blank=True, null=True)
    terminado = models.BooleanField(default=False)
    def __str__(self):
        if self.terminado:
            tmp = ': Recibido: ' + str(self.endda.month) + '/' + str(self.endda.year)
        else:
            tmp = ': Sin terminar'
        return self.titulo + ' en ' + self.establecimiento + tmp

#Habilidades
class AreaConocimiento(models.Model):
    nombre = models.CharField('Nombres', max_length=100)
    def __str__(self):
        return self.nombre

class TipoHabilidad(models.Model):
    area = models.ForeignKey(AreaConocimiento, on_delete=models.CASCADE, related_name="habilidades")
    nombre = models.CharField('Nombres', max_length=100)
    def __str__(self):
        return self.nombre + ' (' + str(self.area) + ')'

class HabilidadAdquirida(models.Model):
    individuo = models.ForeignKey(Individuo, on_delete=models.CASCADE, related_name='habilidades')
    habilidad = models.ForeignKey(TipoHabilidad, on_delete=models.CASCADE, related_name='habilidades')
    nivel = models.IntegerField(choices=NIVEL_HABILIDAD, default=1)
    descripcion = models.CharField('Descripcion', max_length=200, blank=True, null=True)
    def __str__(self):
        return str(self.habilidad) + ': ' + self.get_nivel_display()

if not LOADDATA:
    #Auditoria
    auditlog.register(Individuo)

    auditlog.register(Empleo)
    auditlog.register(ReciboSueldo)
    auditlog.register(Escolaridad)

    auditlog.register(AreaConocimiento)
    auditlog.register(TipoHabilidad)
    auditlog.register(HabilidadAdquirida)