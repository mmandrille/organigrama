#Imports Python
import io
import qrcode
import logging
#Imports Django
from django.dispatch import receiver
from django.shortcuts import redirect
from django.db.models.signals import post_save
#imports Extras.
from PyPDF2 import PdfFileWriter, PdfFileReader
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.utils import ImageReader
#Imports del proyecto
from organigrama.settings import ALLOWED_HOSTS, BASE_DIR
#Imports de la app
from .models import ReciboSueldo

#Logger
logger = logging.getLogger('signals')

#Definimos señales
@receiver(post_save, sender=ReciboSueldo)
def generar_qr(created, instance, **kwargs):
    if created:
        url = redirect('empleados:ver_recibo', recibo_id=instance.pk).url#Generamos el path relativo
        dominio = ALLOWED_HOSTS[0]#Dependemos de que en los settings este bien configurado
        img = qrcode.make(dominio+url)#Generamos la imagen
        #Generamos pin:
        pin = "Pin: " + instance.get_pin()#Idealmente hacer el select_related al llamar esta funcion
        #Se crea un pdf utilizando reportLab
        packet = io.BytesIO()
        pdf = canvas.Canvas(packet, pagesize = A4)
        pdf.setFont('Times-Roman', 12)
        #Le agregamos a pdf_base imagen + pin:
        pdf.drawImage(ImageReader(img.get_image()), 25, 45, 75, 75)
        pdf.drawString(30, 35, pin)
        pdf.save()
        # Nos movemos al comienzo del búfer StringIO
        packet.seek(0)
        nuevo_pdf = PdfFileReader(packet)
        # Leemos el pdf base
        path_recibo = BASE_DIR + instance.archivo.url
        existe_pdf = PdfFileReader(path_recibo, "rb")
        salida = PdfFileWriter()
        # Se agregan lo generado nuevo al existente:
        pagina = existe_pdf.getPage(0)
        pagina.mergePage(nuevo_pdf.getPage(0))
        salida.addPage(pagina)
        # Finalmente se escribe la salida
        outputStream = open(path_recibo, "wb")#Remplazamos ele archivo
        salida.write(outputStream)
        outputStream.close()
