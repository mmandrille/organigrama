#Imports Django
from django.apps import AppConfig
#Imports del Proyecto
from organigrama.settings import LOADDATA
from core.functions import agregar_menu


class EmpleadosConfig(AppConfig):
    name = 'empleados'
    def ready(self):
        agregar_menu(self)
        if not LOADDATA:
            from .signals import generar_qr