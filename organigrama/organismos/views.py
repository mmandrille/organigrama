#Imports de Python
import json
#Imports de Django
from django.utils import timezone
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.admin.views.decorators import staff_member_required
#Imports del proyecto
#Import Personales
from .models import Organismo
from .functions import calcular_ancho

# Create your views here.
def ministerios(request):
    max_child = 1
    origen = Organismo.objects.filter(primario=True).order_by('id').first()
    ancho = calcular_ancho(origen, max_child)
    return render(request, 'organismos_limit.html', {'origen': origen, 'max_child': max_child, 'ancho': ancho}) 

def full_arbol(request):
    max_child = 50
    origen = Organismo.objects.filter(primario=True).order_by('id').first()
    ancho = calcular_ancho(origen, max_child)
    return render(request, 'organismos.html', {'origen': origen, 'ancho': ancho})

def organismo(request, origen_id):
    max_child = 1
    origen = Organismo.objects.get(id=origen_id, activo=True)
    ancho = calcular_ancho(origen, max_child)
    return render(request, 'organismos_limit.html', {'origen': origen, 'max_child': max_child, 'ancho': ancho}) 

def org_limit(request, padre_id, max_child):
    origen = Organismo.objects.get(id=padre_id)
    ancho = calcular_ancho(origen, max_child)
    return render(request, 'organismos_limit.html', {'origen': origen, 'max_child': max_child, 'ancho': ancho})

def organismos_listado(request):
    origen = Organismo.objects.filter(primario=True).order_by('id').first()
    fecha = timezone.now()
    if request.path_info == "/lista":
        edit = False
    else:
        edit = True
    return render(request, 'lista_organigrama/lista.html', {'origen': origen, 'editable': edit, 'fecha': fecha, })

def simple_list(request):
    origen = Organismo.objects.filter(primario=True).order_by('id').first()
    fecha = timezone.now()
    return render(request, 'lista_organigrama/simple_list.html', {'origen': origen, 'fecha': fecha, })

@staff_member_required
def crear_sub_org(request, id_padre):
    new_org = Organismo()
    new_org.padre = Organismo.objects.get(pk=id_padre)
    new_org.nombre = 'SubOrganismo'
    new_org.save()
    return HttpResponseRedirect('/admin/core/organismo/'+ str(new_org.id) + '/change/')

@staff_member_required
def ws_org(request):
    organismos = [org.as_dict() for org in Organismo.objects.filter(activo=True)]
    return HttpResponse(json.dumps({"data": organismos}), content_type='application/json')

@staff_member_required
def ws_org_max(request, padre_id, max_child):
    organismos = Organismo.objects.none()
    org_buscar = Organismo.objects.filter(id=padre_id)

    for x in range(max_child):
        organismos = organismos | org_buscar
        for org in org_buscar:
            org_buscar = org_buscar.exclude(id=org.id)
            org_buscar = org_buscar | Organismo.objects.filter(padre=org)

    organismos = organismos | org_buscar
    organismos = [org.as_dict() for org in organismos]#Lo convertimos en diccionario serialiable
    return HttpResponse(json.dumps({"data": organismos}), content_type='application/json')

@staff_member_required
def ws_organismo(request, organismo_id):
    organismo = Organismo.objects.get(id=organismo_id).as_full_dict()
    return HttpResponse(json.dumps({"data": organismo}), content_type='application/json')