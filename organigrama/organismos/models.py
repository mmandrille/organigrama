from __future__ import unicode_literals
#Imports de la app
from datetime import date
#Imports de Django
from django.db import models
from django.core.files.storage import FileSystemStorage
#Imports extras
from tinymce.models import HTMLField
from auditlog.registry import auditlog
#Imports del proyecto
from organigrama.settings import MEDIA_URL, LOADDATA
from organigrama.constantes import BEGDA, ENDDA
#Imports de la app
from .choices import JERARQUIA

# Create your models here.
class Organismo(models.Model):
    padre = models.ForeignKey('self', on_delete=models.SET_NULL, blank=True, null=True, related_name='hijos')
    nombre = models.CharField('Titulo', max_length=200)
    jerarquia = models.IntegerField(choices=JERARQUIA, default=0)
    descripcion = HTMLField(blank=True, null=True)
    icono = models.ImageField(storage=FileSystemStorage(location=MEDIA_URL), blank=True, null=True)
    direccion = models.CharField('Direccion', max_length=200, blank=True, null=True)
    cuit = models.CharField('CUIT', max_length=13, blank=True, null=True)
    telefonos = models.CharField('Telefonos', max_length=100, blank=True, null=True)
    web = models.URLField('Web', blank=True, null=True)
    color = models.CharField(max_length=7, default="#ffffff")
    activo = models.BooleanField(default=True)
    visible = models.BooleanField(default=True)
    primario = models.BooleanField(default=False)
    def __str__(self):
        if self.padre is not None:
            return self.nombre + ' > ' + self.padre.nombre
        else:
            return self.nombre + ' (Sin Asignar)'
    def as_dict(self):
        if self.padre is None:
            self.padre = self
        return {
            "id": self.id,
            "nombre": self.nombre,
            "padre": self.padre.id,
        }
    def as_full_dict(self):
        return {
            "id": self.id,
            "nombre": self.nombre,
            "descripcion": self.descripcion,
            "icono": self.icono or None,
            "direccion": self.direccion,
            "telefonos": self.telefonos,
            "web": self.web,
        }
    def funcionarios_actuales(self):
        #Obtenemos solo los funcionarios
        funcionarios = self.empleados.exclude(cargo__in=[0, 45,46])#Evitamos los administrativos
        funcionarios = funcionarios.filter(activo=True)
        #Optimizamos
        funcionarios = funcionarios.select_related('individuo')
        return funcionarios
    def save(self, *args, **kwargs):
        if self.primario == True:
            Organismo.objects.all().exclude(pk=self.id).update(primario=False)
        super(Organismo, self).save(*args, **kwargs)

if not LOADDATA:
    #Auditoria
    auditlog.register(Organismo)