def migrar_from_core():
    #Imports Django
    from django.db.models import Q
    #Old Table
    from core.models import Organismo as Old_Org
    from core.models import Funcionario as Old_Func
    #New Table
    from organismos.models import Organismo as New_Org
    from empleados.models import Individuo, Empleo
    #Nos aseguramos de tener Libre tablas nuevas:
    New_Org.objects.all().delete()
    Individuo.objects.all().delete()
    Empleo.objects.all().delete()
    #Generamos Dicts:
    dict_orgs = {}
    new_dict_funcs = {}
    #Print Iniciales:
    cantidad_organismos = Old_Org.objects.count()#Contamos los organismos viejos
    cantidad_funcionarios = Old_Func.objects.all().count()#Contamos los Funcionarios viejos
    print('Organismos viejos: ' + str(cantidad_organismos))
    print('Funcionario viejos: ' + str(cantidad_funcionarios))
    print('Organismos Nuevos Previo Proceso: ' + str(New_Org.objects.count()))
    print('Individuos Nuevos Previo Proceso: ' + str(Individuo.objects.count()))
    print('Empleos Nuevos Previo Proceso: ' + str(Empleo.objects.count()))
    #Procesamos los organismos:
    indice = 0
    print("Iniciamos el procesamiento de Organismos")
    while len(dict_orgs) != cantidad_organismos:
        #Debemos recurrir a este quilombo para solucionar el problema de los padres
        procesar = Old_Org.objects.filter(Q(padre=None)|Q(padre__in=dict_orgs)).exclude(pk__in=dict_orgs)
        #Procesamos los que ya podemos asignar el padre
        for old in procesar:
            #Generamos indicaciones
            indice += 1
            if (indice % 75) == 0:
                print("Procesado", int((100.0/cantidad_organismos)*indice), "% (" + str(indice) + " Organismos)" )
            #Generamos el nuevo organismo
            new = New_Org(
                nombre = old.nombre,
                jerarquia = old.jerarquia,
                descripcion = old.descripcion,
                icono = old.icono,
                direccion = old.direccion,
                cuit = old.cuit,
                telefonos = old.telefonos,
                web = old.web,
                color = old.color,
                activo = old.activo,
                visible = old.visible,
                primario = old.primario,
            )
            if old.padre:#Si tiene padre lo asignamos
                new.padre = dict_orgs[old.padre.pk]
            # Guardamos el nuevo organismo
            new.save()
            dict_orgs[old.pk] = new #Lo dejamos disponible
    #Proceso inicial terminado
    print("Terminamos la generacion de nuevos organismos")
    #Iniciamos la migracion de Funcionarios
    #Procesamos los Funcionarios
    print("Iniciamos el procesamiento de Funcionarios")
    new_funcs = {}
    indice = 0
    for old in Old_Func.objects.all():#Generamos los nuevos Funcionarios
        #Generamos indicaciones
        indice += 1
        if (indice % 75) == 0:
            print("Procesado", int((100.0/cantidad_funcionarios)*indice), "% (" + str(indice) + " Funcionarios)" )
        #Dado que no habian cargado el dni:
        if not old.dni:
            old.dni = old.pk#Mentimos pero es mejor que nada.
        #Creamos el individuo
        individuo = Individuo(
            num_doc = old.dni,
            apellidos = old.apellidos,
            nombres = old.nombres,
            telefono = old.telefono,
            email = old.email,
            fotografia = old.foto,
            titulo = old.titulo,
        )
        individuo.save()
        new_funcs[old.dni] = individuo
        #Creamos el cargo:
        empleo = Empleo(
            organismo = dict_orgs[old.organismo.pk],
            individuo = new_funcs[old.dni],
            cargo = old.cargo,
            subcargo = old.subcargo,
            decreto = old.decreto,
            funcion_unica = old.funcion_unica,
            begda = old.begda,
            endda = old.endda,
            activo = old.activo,
        )
        empleo.save()
    #Proceso Final terminado
    print("Terminamos la generacion de nuevos Individuos y Empleos")
    #Informamos resultado
    print('Organismos Nuevos Post Proceso: ' + str(New_Org.objects.count()))
    print('Funcionario Nuevos Post Proceso: ' + str(New_Func.objects.count()))
