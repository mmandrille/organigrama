#Imports django
from django.contrib.auth.models import User
#Imports Extras
from dal import autocomplete
#Imports de la app
from .models import Organismo

class OrganismoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Organismo.objects.all()
        if self.q:
            qs = qs.filter(nombre__icontains=self.q)
        return qs