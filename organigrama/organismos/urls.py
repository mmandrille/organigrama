#Imports Django
from django.conf.urls import url
from django.urls import path
#imports del proyecto
from . import views
from . import autocomplete

app_name = 'organismos'
urlpatterns = [
    #Personales
    path('', views.ministerios, name='ministerios'),
    path('full', views.full_arbol, name='full_arbol'),
    path('<int:origen_id>', views.organismo, name='organismo'),
    path('origen/<int:padre_id>/max/<int:max_child>/', views.org_limit, name='organismos_limit'),
    
    #Generar Listado
    path('lista', views.organismos_listado, name='organismos_pdf'),
    path('lista_edit', views.organismos_listado, name='organismos_pdf'),
    path('simple_list', views.simple_list, name='simple_list'),

    #Edicion
    path('crear_hijo/<int:id_padre>/', views.crear_sub_org, name='crear_sub_org'),
    
    #Autocomplete
    url(r'^organismos-autocomplete/$', autocomplete.OrganismoAutocomplete.as_view(), name='organismos-autocomplete',),

    #Web Services
    path('ws_org/', views.ws_org, name='ws_org'),
    path('ws_org/origen/<int:padre_id>/max/<int:max_child>/', views.ws_org_max, name='ws_org_max'),
    path('ws_organismo/<int:organismo_id>', views.ws_organismo, name='ws_organismo'),
]