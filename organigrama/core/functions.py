#Imports de python
import re
import traceback
#Imports django
from django.utils import timezone
from django.http import JsonResponse
from django.db.models.deletion import Collector
#Imports extras:
#Imports de la app
from .apps import CoreConfig

#Funciones para asignar a las app.configs    
def agregar_menu(app):
    CoreConfig.ADMIN_MENU += [(app.name.capitalize() , app.name)]

def is_related(instance):
    collector = Collector(using="default")
    collector.collect([instance])
    if collector.dependencies:
        return True

def json_error(error, vista, logger, data):
    #Guardamos el error
    logger.info("\n"+str(timezone.now())+'\n'+vista+":\n"+"|"+str(data))
    logger.info("Falla: "+str(error)+'\n'+str(traceback.format_exc()))
    #Respondemos al device
    return JsonResponse(
            {
                "accion": vista,
                "realizado": False,
                "error": str(error),
            },
            safe=False,
            status=400,
        )