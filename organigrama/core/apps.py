#Imports Django
from django.apps import AppConfig
#Imports del Proyecto
from organigrama.settings import LOADDATA

class CoreConfig(AppConfig):
    name = 'core'
    ADMIN_MENU = []
    ADMIN_MODELS = {}
    def ready(self):
        if not LOADDATA:
            from .signals import enviar_mail_new_user