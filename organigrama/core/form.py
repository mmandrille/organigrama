#Imports Django
from django.forms import ModelForm
from django.forms.widgets import TextInput
#Imports Extras
from dal import autocomplete
#Imports del Proyecto
from .models import Organismo

#Definimos Forms
class OrganismoForm(ModelForm):
    class Meta:
        model = Organismo
        fields = '__all__'
        widgets = {
            'color': TextInput(attrs={'type': 'color'}),
            'padre': autocomplete.ModelSelect2(url='organismos:organismos-autocomplete'),
        }