#Imports Django
from django.conf.urls import url
from django.urls import path
#imports del proyecto
from . import views
from . import autocomplete

app_name = 'core'
urlpatterns = [
    path('', views.home, name='home'),
    #Acceso de usuarios
    path('login', views.home_login, name='home_login'),
    path('logout', views.home_logout, name='home_logout'),
    #Autocomplete
    url(r'^usuarios-autocomplete/$', autocomplete.UsuariosAutocomplete.as_view(), name='usuarios-autocomplete',),
]