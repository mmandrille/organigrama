#Imports de Python
import json
#Imports de Django
from django.utils import timezone
from django.shortcuts import render
from django.contrib.auth import authenticate
from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm
#Import Personales

# Create your views here.
def home(request):
    return render(request, 'home.html', {})

#Manejo de sesiones de Usuarios
def home_login(request):
    message = ''
    form = AuthenticationForm()
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        form = AuthenticationForm(data=request.POST)
        # Si el formulario es válido...
        if form.is_valid():
            # Recuperamos las credenciales validadas
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            # Verificamos las credenciales del usuario
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
                return home(request)
    return render(request, "extras/generic_form.html", {'titulo': "Ingresar al Sistema", 'form': form, 'boton': "Ingresar", 'message': message, })

def home_logout(request):
    logout(request)
    return home(request)