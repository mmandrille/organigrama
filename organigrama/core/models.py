from __future__ import unicode_literals
#Imports de la app
from datetime import date
#Imports de Django
from django.db import models
from django.core.files.storage import FileSystemStorage
#Imports extras
from tinymce.models import HTMLField
#Imports del proyecto
from organigrama.settings import MEDIA_URL, LOADDATA
from organigrama.constantes import BEGDA, ENDDA
#Imports de la app
from organismos.choices import JERARQUIA
from empleados.choices import CARGOS

# Create your models here.
class Organismo(models.Model):
    padre = models.ForeignKey('self', on_delete=models.SET_NULL, blank=True, null=True, related_name='hijos')
    nombre = models.CharField('Titulo', max_length=200)
    jerarquia = models.IntegerField(choices=JERARQUIA, default=0)
    descripcion = HTMLField(blank=True, null=True)
    icono = models.ImageField(storage=FileSystemStorage(location=MEDIA_URL), blank=True, null=True)
    direccion = models.CharField('Direccion', max_length=200, blank=True, null=True)
    cuit = models.CharField('CUIT', max_length=13, blank=True, null=True)
    telefonos = models.CharField('Telefonos', max_length=100, blank=True, null=True)
    web = models.URLField('Web', blank=True, null=True)
    color = models.CharField(max_length=7, default="#ffffff")
    activo = models.BooleanField(default=True)
    visible = models.BooleanField(default=True)
    primario = models.BooleanField(default=False)
    def __str__(self):
        if self.padre is not None:
            return self.nombre + ' > ' + self.padre.nombre
        else:
            return self.nombre + ' (Sin Asignar)'
    def as_dict(self):
        if self.padre is None:
            self.padre = self
        return {
            "id": self.id,
            "nombre": self.nombre,
            "padre": self.padre.id,
        }
    def as_full_dict(self):
        return {
            "id": self.id,
            "nombre": self.nombre,
            "descripcion": self.descripcion,
            "icono": self.icono or None,
            "direccion": self.direccion,
            "telefonos": self.telefonos,
            "web": self.web,
        }
    def funcionario_actual(self):
        return self.funcionarios.filter(activo=True)
    def save(self, *args, **kwargs):
        if self.primario == True:
            Organismo.objects.all().exclude(pk=self.id).update(primario=False)
        super(Organismo, self).save(*args, **kwargs)

class Funcionario(models.Model):
    organismo = models.ForeignKey(Organismo, on_delete=models.SET_NULL, related_name='funcionarios', blank=True, null=True)
    cargo = models.IntegerField(choices=CARGOS, default=0)
    subcargo = models.CharField('SubCargo', max_length=20, blank=True, null=True)
    funcion_unica = models.BooleanField(default=False)
    nombres = models.CharField('Nombres', max_length=100)
    apellidos = models.CharField('Apellidos', max_length=100)
    foto = models.ImageField(storage=FileSystemStorage(location=MEDIA_URL), blank=True, null=True)
    dni = models.CharField('DNI', max_length=100, blank=True, null=True)
    titulo = models.CharField('Titulo Profesional', max_length=20)
    email = models.EmailField('Correo Personal', blank=True, null=True)
    telefono = models.CharField('Telefono', max_length=20, blank=True, null=True)
    decreto = models.IntegerField(blank=True, null=True)
    begda = models.DateField('Designacion',  default=BEGDA)
    endda = models.DateField('Cese de Funciones', default=ENDDA)
    activo = models.BooleanField(default=True)
    def __str__(self):
        if self.organismo is None: 
            return 'Sin Organismo > ' + self.nombres + ' ' + self.apellidos
        else:
            return self.organismo.nombre + ' > ' + self.nombres + ' ' + self.apellidos
    def as_dict(self):
        return {
            "organismo": self.organismo.id,
            "nombres": self.nombres,
            "apellidos": self.apellidos,
            "cargo": dict(CARGOS).get(self.cargo),
            "dni": self.dni,
            "foto": self.foto,
            "titulo": self.titulo,
            "email": self.email,
            "telefono": self.telefono,
            "decreto": self.decreto,
        }
    def save(self, *args, **kwargs):
        if self.activo == True and self.funcion_unica:#tener en cuenta otros funcionarios
            self.organismo.funcionarios.exclude(pk=self.pk).filter(cargo=self.cargo,subcargo=self.subcargo).update(endda=self.begda, activo=False)
        super(Funcionario, self).save(*args, **kwargs)

if not LOADDATA:
    from .signals import enviar_mail_new_user
